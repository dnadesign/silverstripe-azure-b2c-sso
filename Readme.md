
# Silverstripe Azure B2C Single Sign-on module

This module is intead to facilitate the usage of Azure B2C Single Sign-on with your site.

## Setup

Run `dev/build` so that the required fields are added to the database.

### **Env configuration**

Required env variables.

```.env
AZUREB2C_CONFIG_URL=''
AZUREB2C_TENANT_ID=''
AZUREB2C_CLIENT_ID=''
AZUREB2C_CLIENT_SECRET=''
AZUREB2C_RESET_PASSWORD_POLICY=''
AZUREB2C_DEFAULT_ENDPOINT_VERSION=''
```

### **YML configuration**

You'll need to set up specific configurations relevant to your site and single sign-on process

#### **BigFork Authenticator**

Please follow BigFork's [instructions](https://github.com/bigfork/silverstripe-oauth-login#configuration) for setup. Note that `SilverStripe\Core\Injector\Injector` is being set by this module specifically for AzureB2C.
```yml
---
Name: config-name
After:
  - '#app-auth-reset'
  - '#sso-config'
  - '#sso-config-dev-reset'
---

Bigfork\SilverStripeOAuth\Client\Authenticator\Authenticator:
  providers:
    AzureB2CProvider:
      name: 'Azure B2C login'
      scopes:
        - YOUR
        - SCOPE
        - ARRAY
```

#### **Cookie**

Set up cookie name and domains.

```yml
---
Name: module-cookies
After:
  - '*'
---
SSO\Control\Session:
  cookie_name: 'COOKIENAME'
  cookie_domains:
    - 'example1'
    - 'example2.com'
```

#### **Cors -** *Optional*

You may want to set up CORS for AzureB2C.

```yml
---
Name: module-cors
After:
  - '*'
---
SSO\Control\AzureB2CTemplateController:
  cors:
    Enabled: true
    Allow-Origin:
        - 'https://example1.com'
        - 'https://example2.com'
        - 'https://example3.com'
    Allow-Headers: '*'
    Expose-Headers: '*'
    Allow-Methods: 'GET, OPTIONS'
    Max-Age: 600
```

#### **Error messages**

Error messages should be handled via yml config. By default LoginErrorHandler will look for lookup messages, set those in your yml config.

```yml
---
Name: sso-exception
After:
  - '*'
---
SSO\Client\Handler\LoginErrorHandler:
  lookup:
    'Messages go here'
```

### **Templates**

You should override the default templates for your own usage.

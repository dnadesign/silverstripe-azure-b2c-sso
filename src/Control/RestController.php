<?php

namespace SSO\Control;

use Bigfork\SilverStripeOAuth\Client\Model\Passport;
use SilverStripe\Security\Member;
use SilverStripe\ORM\ValidationException;
use SilverStripe\Core\Environment;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Control\HTTPResponse_Exception;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Convert;

class RestController extends Controller
{
    use Configurable;

    /**
     * Data from request
     *
     * @var array
     */
    protected $data = [];

    /**
     *
     *
     * @param HTTPRequest $request
     * @return HTTPResponse
     */
    public function index(HTTPRequest $request)
    {
        $this->authenticate();

        try {
            $this->validateRequest();
            return $this->deleteSilverStripeUser();
        } catch (\Exception $e) {
            $body = [
                'message' => $e->getMessage()
            ];

            $json = json_encode($body);

            $this->response
                ->setStatusCode(400)
                ->setbody($json)
                ->addHeader('content-type', 'application/json');
            return $this->response;
        }
    }

    /**
     * Authenticate the request
     *s
     */
    public function authenticate()
    {
        // if Live env, use from Env Vars
        if (!Environment::getEnv('USER_API_AUTH_USER')) {
            throw new \Exception('No Basic Auth user env set');
        }

        if (!Environment::getEnv('USER_API_AUTH_PASSWORD')) {
            throw new \Exception('No Basic Auth pass env set');
        }

        $user = Environment::getEnv('USER_API_AUTH_USER');
        $pass = Environment::getEnv('USER_API_AUTH_PASSWORD');

        //standard authorisation header
        if ($this->request->getHeader('Authorization')) {
            $header = $this->request->getHeader('Authorization');
            $token = trim(str_replace('Basic ', '', $header));
            $decoded = base64_decode($token);
            $parts = explode(':', $decoded);
            if (count($parts) === 2
                && $parts[0] === $user
                && $parts[1] === $pass
            ) {
                return true;
            }
        }
        //guzzle basic auth headers
        if ($this->request->getHeader('php_auth_user') && $this->request->getHeader('php_auth_pw')) {
            if(
                $user = $this->request->getHeader('php_auth_user') &&
                $pass = $this->request->getHeader('php_auth_pw')
            ){
                return true;
            }
        }

        $response = new HTTPResponse(null, 401);
        $response->setBody('Unauthorised');

        // Exception is caught by RequestHandler->handleRequest() and will halt further execution
        $e = new HTTPResponse_Exception(null, 401);
        $e->setResponse($response);
        throw $e;
    }

    /**
     * Validate the request
     * - Check request body exists
     * - check body is json
     * - check azureID is set
     * @return void
     */
    public function validateRequest()
    {
        if (!$this->request->isPOST()) {
            throw new ValidationException('Disallowed http request method.', 500);
        }

        $body = $this->request->getBody();

        if (!$body || empty($body)) {
            throw new ValidationException('No body provided', 500);
        }

        $this->data = json_decode($body, true);

        if (json_last_error()) {
            throw new ValidationException('Bad json provided', 500);
        }


        if (!isset($this->data['azureID'])) {
            throw new ValidationException('Azure ID is missing', 500);
        }
    }


    /**
     * Process the request
     * -find a member giver their Azure ID
     * -delete the user and all related user data
     * -return a json response
     * @return HTTPResponse
     */
    public function deleteSilverStripeUser()
    {
        $azureID = Convert::raw2sql($this->data['azureID']);
        $passport = Passport::get()->filter(['Identifier' => $azureID])->first();

        if(!$passport){
            throw new \Exception('Passport for Azure ID could not be found.');
        }

        $memberID = $passport->MemberID;
        $member = Member::get()->byID($passport->MemberID);

        if(!$member){
            throw new \Exception('Passport for Azure ID was found, but the related Member record could not be found.');
        }

        try {
            $member->delete();
            $member->destroy();
            $body = [
                'success' => 'Member with ID (' . $memberID . ') and Azure ID (' . $azureID . ') was successfully deleted.',
                'memberID' => $memberID,
                'passportIDP' => $passport->IDP,
            ];
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        $this->response
            ->setStatusCode(200)
            ->setbody(json_encode($body))
            ->addHeader('content-type', 'application/json');

        return $this->response;
    }
}

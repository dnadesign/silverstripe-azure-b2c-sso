<?php

namespace SSO\Control;

use SilverStripe\Core\Injector\Injectable;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\Director;
use SilverStripe\Control\Cookie;
use SSO\Helper\Helper;
use SSO\Client\Token\AccessToken;

/**
 * Bespoke session helper class to store cookies and cookie settings
 */
class Session
{
    use Configurable;
    
    use Injectable;

    /**
     * Cookie name
     *
     * @var string
     */
    private static $cookie_name = null;

    /**
     * Contingecy - this cookie will expire this amount of time
     * before trhe azure access token will expire
     *
     * @var int
     */
    private static $cookie_expiry_contingency = 5 * 60; // 5 minutes

    /**
     * Cookie domains
     *
     * @var array
     */
    private static $cookie_domains = [];

    /**
     * Set the SSO Cookie
     *
     * @return Cookie
     */
    public function getCookie()
    {
        return Cookie::get($this->config()->get('cookie_name'));
    }

    /**
     * Set the SSO Cookie
     *
     * @param AccessToken $accessToken Used to see how long the Cookie should last
     * Should match the lifetime minus some contigency
     *
     * @return void
     */
    public function setCookie(AccessToken $accessToken, HTTPRequest $request)
    {
        $secondsUntilExpiry = $this->calculateExpiry($accessToken);
        $daysUntilExpiry = $this->calculateExpiryDays($accessToken);

        $name = $this->config()->get('cookie_name');
        $domain = self::get_cookie_domain();

        // set the domain level SSO cookie
        Cookie::set(
            $name, // name
            1, // value
            $daysUntilExpiry, // expiry in days
            '/', // path
            $domain, // domain
            true, // secure
            true // httpOnly (disable to allow a session via js)
        );
    }

    /**
     * Determine the Domain to be used for the cookie
     * - Using config first
     * - fallback to the TLD of the current URL
     *
     * @return void
     */
    public static function get_cookie_domain()
    {
        $url = Director::absoluteBaseURL();
        $domains = Config::inst()->get(self::class, 'cookie_domains');

        foreach ($domains as $domain) {
            if (stristr($url, $domain)) {
                return $domain;
            }
        }

        // fallback - try to ascertain the TLD
        return Helper::get_domain();
    }

    /**
     * Calculate the time (in days) that the cooke should live for
     * Deduct some contingency so that the cookie expires just before the accessToken.
     *
     * @param AccessToken $accessToken
     * @return float Number of Days (could be a decimal) until cookie should expire
     */
    public function calculateExpiryDays(AccessToken $accessToken)
    {
        $diff = $this->calculateExpiry($accessToken);
        $days = $diff / (60 * 60 * 24);
        
        return $days;
    }

    /**
     * Calculate the expiry in seconds from the access token
     * Also subtract a small amount of contingency to that our local
     * expiry is always slightly before the azure B2C token expiry
     *
     * @param AccessToken $accessToken
     * @return int Seconds until expiry
     */
    public function calculateExpiry(AccessToken $accessToken)
    {
        $then = $accessToken->getExpires($accessToken);
        $now = time();
        $expiryContingency = $this->config()->get('cookie_expiry_contingency');
        $diff = $then - $now - $expiryContingency;
        
        return $diff;
    }

    /**
     * Force thr SSO session to expire
     * Should be called when a iser logs out of Silverstripe and Azure
     *
     * @return void
     */
    public function clear()
    {
        $name = $this->config()->get('cookie_name');
        $domain = self::get_cookie_domain();
    
        Cookie::force_expiry(
            $name,
            '/',
            $domain,
            true,
            true
        );
    }
}

<?php

namespace SSO\Control;

use SilverStripe\CMS\Controllers\ContentController;

/**
 * Template to provide templates and assets (via CORS) is you choose to
 * add your own templates to Azure B2C. Just provide Azure B2C to this template.
 * Add a route to this controlelr in your yml, and then extend this controller with
 * your own actions
 */
class AzureB2CTemplateController extends ContentController
{

    /**
     * @var array
     */
    private static $allowed_actions = [];

    /**
     * @var array
     */
    private static $url_handlers = [];

    /**
     * Basic Cors setup
     *
     * @var array
     */
    private static $cors = [];


    /**
     * Index - this will actually be the action used by azure to render the template
     * The other actions are just for local development
     *
     * @return void
     */
    public function index()
    {
        return $this;
    }
}

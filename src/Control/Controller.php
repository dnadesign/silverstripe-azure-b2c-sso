<?php

namespace SSO\Control;

use SilverStripe\Security\Security;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Core\Injector\Injectable;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTP;
use SilverStripe\Control\Director;
use SSO\Helper\Helper;
use SSO\Client\Handler\LogoutHandler;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\AbstractProvider;
use Bigfork\SilverStripeOAuth\Client\Factory\ProviderFactory;
use Bigfork\SilverStripeOAuth\Client\Control\Controller as SilverStripeOAuthController;

/**
 * Main controller to handle /oauth and /callback actions for SSO
 */
class Controller extends SilverStripeOAuthController
{
    use Configurable;
    
    use Injectable;

    /**
     * URL to local login
     * This will in turn redirtect to the azure login endpoint
     *
     * @var string
     */
    private static $login_url = '/oauth/login';

    /**
     * URL to local logout
     * This will in turn redirtect to the azure log out endpoint (SLO)
     *
     * @var string
     */
    private static $logout_url = '/oauth/logout';

    /**
     * @var array
     */
    private static $allowed_actions = [
        'login',
        'logout',
        'authenticate',
        'callback'
    ];

    /**
     * @var array
     */
    private static $url_handlers = [
        'login' => 'login',
        'logout' => 'logout',
        'authenticate' => 'authenticate',
        'callback' => 'callback'
    ];

    /**
     * This takes parameters like the provider, scopes and callback url, builds an authentication
     * url with the provider's site and then redirects to it
     *
     * Extends parent::authenticate in order to handle logout as well
     *
     * @param HTTPRequest $request
     * @return HTTPResponse
     * @throws HTTPResponse_Exception
     */
    public function authenticate(HTTPRequest $request)
    {
        $providerName = $request->getVar('provider');
        $context = $request->getVar('context');
        $scope = $request->getVar('scope');

        // Missing or invalid data means we can't proceed
        if (!$providerName || !is_array($scope)) {
            $this->httpError(404);
            return null;
        }

        /** @var ProviderFactory $providerFactory */
        $providerFactory = Injector::inst()->get(ProviderFactory::class);
        $provider = $providerFactory->getProvider($providerName);
        if ($context === 'login') {
            $url = $provider->getAuthorizationUrl(['scope' => $scope]);
        } elseif ($context === 'logout') {
            $url = $provider->getLogoutUrl(['scope' => $scope]);
        }

        $request->getSession()->set('oauth2', [
            'state' => $provider->getState(),
            'provider' => $providerName,
            'context' => $context,
            'scope' => $scope,

            // this is the url the user is sent back to after all
            // post auth actions are completed (if any)
            'backurl' => $this->findBackUrl($request)
        ]);

        return $this->redirect($url);
    }

    /**
     * The return endpoint after the user has authenticated with a provider
     *
     * @param HTTPRequest $request
     * @return HTTPResponse
     * @throws HTTPResponse_Exception
     */
    public function callback(HTTPRequest $request)
    {
        $session = $request->getSession();
        $context = $session->get('oauth2.context');

        // fall back context to login
        if (!$context) {
            $context = 'login';
        }

        $providerName = Helper::get_authenticator_name();
        $providerFactory = Injector::inst()->get(ProviderFactory::class);
        $provider = $providerFactory->getProvider($providerName);
   
        // @todo at the moment this will print an unfreindly error.
        // ideally this should redirect to a better error page
        // or try login to azure again
        try {
            if ($request->getVar('error') && $request->getVar('error_description')) {
                throw new \Exception('Authentication error', 500);
            }
            
            if (!$this->validateState($request)) {
                throw new \Exception('Invalid session state', 500);
            }

            if ($context === 'login') {
                $this->doLogin($request, $provider);
            } else {
                $this->doLogout($request);
            }
        } catch (IdentityProviderException $e) {
            /** @var LoggerInterface $logger */
            $logger = Injector::inst()->get(LoggerInterface::class . '.oauth');
            $logger->error('OAuth IdentityProviderException: ' . $e->getMessage());
            throw new \Exception('Invalid access token', 500);
            return null;
        } catch (\Exception $e) {
            $errorHandlers = $this->getHandlersForContext($context, 'error_handlers');
            if (empty($errorHandlers)) {
                throw new \Exception('No handerls for context');
            }
 
            // Run handlers to process the error message
            foreach ($errorHandlers as $errorHandlerConfig) {
                $errorHandler = Injector::inst()->create($errorHandlerConfig['class']);
                $result = $errorHandler->handleError($provider, $request, $e);

                if ($result instanceof HTTPResponse) {
                    return $result;
                }
            }
        }

        $returnUrl = null;
        if ($context === 'login') {
            // login should always go through post auth url
            $returnUrl = $this->getPostAuthUrl();
        }

        // all other context, or if no postAuthUrl, use BackUrl and clear session
        if (!$returnUrl) {
            $returnUrl = $this->getReturnUrl();
            $session->clear('oauth2');
        }
    
        return $this->redirect($returnUrl);
    }

    /**
     * Get the url to redirect to after login
     *
     * @return string
     */
    protected function getPostAuthUrl()
    {
        // check for a post-azure controller and redirect there
        if ($postAuthControllerClass = Config::inst()->get(self::class, 'post_auth_controller_class')) {
            $postAuthController = Injector::inst()->create($postAuthControllerClass);
            $returnUrl = Director::absoluteURL($postAuthController->Link());

            return $returnUrl;
        }

        return null;
    }


    /**
     * Get the return URL previously stored in session
     *
     * @return string
     */
    protected function getReturnUrl()
    {
        $returnUrl = $this->getRequest()->getSession()->get('oauth2.backurl');
        
        if (!$returnUrl || !Director::is_site_url($returnUrl)) {
            $returnUrl = Director::absoluteBaseURL();
        }

        return $returnUrl;
    }

    /**
     * Handle the response from Azure B2C for Logging in
     *
     * @param HTTPRequest $request
     * @param AbstractProvider $provider
     * @param string $returnUrl
     * @return void
     */
    public function doLogin(HTTPRequest $request, AbstractProvider $provider)
    {
        $session = $request->getSession();
        $accessToken = $provider->getAccessToken('authorization_code', [
            'code' => $request->getVar('code')
        ]);

        $handlers = $this->getHandlersForContext('login');

        // Run handlers to process the token
        $results = [];
        foreach ($handlers as $handlerConfig) {
            $handler = Injector::inst()->create($handlerConfig['class']);
            $results[] = $handler->handleToken($accessToken, $provider);
        }

        // Handlers may return response objects
        foreach ($results as $result) {
            if ($result instanceof HTTPResponse) {
                $session->clear('oauth2');

                // If the response is redirecting to the login page (e.g. on Security::permissionFailure()),
                // update the BackURL so it doesn't point to /oauth/callback/
                if ($result->isRedirect()) {
                    $location = $result->getHeader('location');
                    $relativeLocation = Director::makeRelative($location);

                    // If the URL begins Security/login and a BackURL parameter is set...
                    if (
                        strpos($relativeLocation, Security::config()->uninherited('login_url')) === 0
                        && strpos($relativeLocation, 'BackURL') !== -1
                    ) {
                        $returnUrl = $this->getReturnUrl();
                        $session->set('BackURL', $returnUrl);
                        $location = HTTP::setGetVar('BackURL', $returnUrl, $location);
                        $result->addHeader('location', $location);
                    }
                }

                return $result;
            }
        }
    }

    /**
     * Handle the response from Azure B2C for a logout
     *
     * @param HTTPRequest $request
     * @return void
     */
    public function doLogout(HTTPRequest $request)
    {
        // store return url for after logout as logout will destroy current session
        $returnUrl = $request->getSession()->get('oauth2.backurl');
        // logout
        $handler = Injector::inst()->create(LogoutHandler::class);
        $handler->handleLogout($request);
        
        // save returnUrl back to new session
        $request->getSession()->set('oauth2.backurl', $returnUrl);
    }

    /**
     * Login to Azure
     * Redirects the user to the built login URL
     *
     * @return void
     */
    public function login()
    {
        if (Security::getCurrentUser()) {
            return $this->redirectBack();
        }

        $request = $this->getRequest();
        $url = Helper::getProviderLoginURL($request);

        return $this->redirect($url);
    }

    /**
     * Logout of Azure
     * Redirects the user to the built logout URL
     *
     * @return void
     */
    public function logout()
    {
        if (!Security::getCurrentUser()) {
            return $this->redirectBack();
        }

        $request = $this->getRequest();
        $url = Helper::getProviderLogoutURL($request);

        return $this->redirect($url);
    }

    /**
     * Redirect back to the provided BackURL
     * Otherwise redirect back to the homepage
     *
     * @return void
     */
    public function redirectBack()
    {
        $request = $this->getRequest();
        $backURL = $request->requestVar('BackURL');
        if ($backURL && Director::is_site_url($backURL)) {
            return $this->redirect($backURL);
        }

        return $this->redirect('/');
    }

    /**
     * Logic copied from \SilverStripe\Control\Controller::redirectBack()
     *
     * @param HTTPRequest $request
     * @return string|null
     */
    protected function findBackUrl(HTTPRequest $request)
    {
        if ($request->getSession() && $request->getSession()->get('BackURL')) {
            $backUrl = $request->getSession()->get('BackURL');
        } elseif ($request->requestVar('BackURL')) {
            $backUrl = $request->requestVar('BackURL');
        } elseif ($request->isAjax() && $request->getHeader('X-Backurl')) {
            $backUrl = $request->getHeader('X-Backurl');
        } elseif ($request->getHeader('Referer')) {
            $backUrl = $request->getHeader('Referer');
        } else {
            $backUrl = null;
        }

        if (!$backUrl || !Director::is_site_url($backUrl)) {
            $backUrl = Director::absoluteBaseURL();
        }

        return $backUrl;
    }
}

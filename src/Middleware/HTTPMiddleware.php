<?php

namespace SSO\Middleware;

use SilverStripe\Security\Security;
use SilverStripe\Security\Permission;
use SilverStripe\Security\Member;
use SilverStripe\Security\IdentityStore;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\Middleware\HTTPMiddleware as SilverstripeHTTPMiddleware;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\Director;
use SSO\Control\Session;
use SSO\Helper\Helper;
use SSO\Control\Controller;

/**
 * This is where all the SSO (cross site logins) happens.
 * This uses HTTP Middleware to detect a singfle sign on session:
 * Redirect to Azure to check login if:
 * - user is not logged in
 * - a login cookie exists
 * @see
 */
class HTTPMiddleware implements SilverstripeHTTPMiddleware
{
    use Configurable;

    /**
     * @var bool Whether this middleware is enabled or not
     * @config
     */
    private static $enabled = true;

    /**
     * @var array A list of URLs to be excluded from processing via this middleware. These URLs are passed through
     * preg_match(), so regular expressions are acceptable. The default URLs include:
     * - Any URL under Security/
     * - Any URL under oauth/
     * - Any URL under admin/
     */
    private static $excluded_urls = [];

    /**
     * For every request, we check if a user is logged in to azure
     * If the SSO cookie is set, then the user needs to be logged into azure (except if admin access)
     *
     * @param HTTPRequest $request
     * @param Object $delegate
     * @return void
     */
    public function process(HTTPRequest $request, $delegate)
    {
        // If the middleware isn't enabled, immediately stop processing and pass on to other delegates
        if (!$this->isEnabled()) {
            return $delegate($request);
        }

        // Don't redirect on CLI
        if (Director::is_cli()) {
            return $delegate($request);
        }

        // if on an admin or Security page then
        // reset the orioginal login forms
        $adminUrls = $this->getExcludedAdminUrls();
        if ($this->checkExcludedUrl($request, $adminUrls)) {
            // dont include the default Security login/out to retain access to CMS by Admins
            Config::inst()->update(Security::class, 'login_url', 'Security/login');
            Config::inst()->update(Security::class, 'logout_url', 'Security/logout');
            return $delegate($request);
        }

        // Check the URL to see if it matches an exclusion rule - if so, stop processing and
        // pass on to other delegates
        $urls = $this->getExcludedUrls();
        if ($this->checkExcludedUrl($request, $urls)) {
            return $delegate($request);
        }

        // If sso_session_action session variable is set, then this request is a part of a
        // login our logout flow and the normal delegate should be returned
        if ($request->getSession()->get('sso_session_action')) {
            $request->getSession()->clear('sso_session_action');
            return $delegate($request);
        }

        // get the sso cookie
        $cookie = Injector::inst()->create(Session::class)->getCookie();

        // there is a mebmer, but no cookie
        // if the user isnt an admin, or has no CMS access
        // log them out of ss (and therefore Azure)
        $member = Security::getCurrentUser();
        if (!$cookie && $member && !$this->hasAdminAccess($member)) {
            $response = $this->logOutUser($request);

            if ($response) {
                return $response;
            }

            return $delegate($request);
        }

        // there is a cookie, but no member they have logged
        // in somewhere else on the same domain so log them in.
        if (!$member && $cookie) {
            return $this->logInUser($request);
        }

        return $delegate($request);
    }

    /**
     * Admins dont need to be logged in via azure
     *
     * @return void
     */
    protected function hasAdminAccess(Member $member)
    {
        $perms = ['ADMIN', 'CMS_ACCESS'];

        foreach ($perms as $perm) {
            if (Permission::checkMember($member, $perm)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Send the user to the Azure auth page
     * This will either return an existing logged in state
     * or present the user with the azure login form
     *
     * @param HTTPRequest $request
     * @return void
     */
    public function logInUser(HTTPRequest $request)
    {
        $url = Helper::getProviderLoginURL($request);
        $controller = Injector::inst()->createWithArgs(Controller::class, [$request]);
        return $controller->redirect($url);
    }

    /**
     * Log the user out. This will:
     * - log the user out of Silverstripe
     * - log out of azure (via onAfterlOggedOut extension hook)
     * - delete the SSO cooie
     *
     * @param HTTPRequest $request
     * @return void
     */
    public function logOutUser(HTTPRequest $request)
    {
        return Injector::inst()->get(IdentityStore::class)->logOut($request);
    }

    /**
     * Check the Config for excluded URLs
     *
     * @param HTTPRequest $request The current request
     * @return bool true if the current URL should be excluded from having this middleware run
     */
    protected function checkExcludedUrl(HTTPRequest $request, array $urls)
    {
        $currentRelativeUrl = $request->getURL(true);

        foreach ($urls as $pattern) {
            if (preg_match($pattern, $currentRelativeUrl)) {
                return true;
            }
        }

        // If no URLs match, then the current URL isn't excluded and should be processed
        return false;
    }

    /**
     * @return array The list of all excluded URLs
     */
    protected function getExcludedUrls()
    {
        return $this->config()->excluded_urls;
    }

    /**
     * @return array The list of all excluded admin URLs
     */
    protected function getExcludedAdminUrls()
    {
        return $this->config()->excluded_admin_urls;
    }

    /**
     * @return bool true if this middleware is enabled, false if it's not enabled
     */
    protected function isEnabled()
    {
        return $this->config()->enabled;
    }
}

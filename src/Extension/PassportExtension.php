<?php

namespace SSO\Extension;

use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\ORM\FieldType\DBText;
use SilverStripe\ORM\DataExtension;

class PassportExtension extends DataExtension
{
    private static $db = [
        'IDP' => DBVarchar::class,
        'AccessToken' => DBText::class
    ];

    private static $summary_fields = [
        'ID' => 'ID',
        'Identifier' => 'Identifier',
        'IDP' => 'IDP'
    ];
}

<?php

namespace SSO\Extension;

use SilverStripe\Core\Injector\Injector;
use SilverStripe\Core\Extension;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\Controller;
use SSO\Client\Token\AccessToken;
use SSO\Control\Session;
use SSO\Helper\Helper;

class MemberExtension extends Extension
{
    /**
     * Clear the SSO cookie when a user logs out
     * We use afterMemberLoggedOut because we want the cookie cleared
     * for _every_ user
     *
     * @param HTTPRequest $request
     */
    public function afterMemberLoggedOut(HTTPRequest $request)
    {
        // if there is an SSO cookie, redirect to Azure to log the user out there
        $session = Injector::inst()->get(Session::class);

        if ($session->getCookie()) {
            // clear the cookie
            $session->clear();
        }
    }

    /**
     * Get passport for user
     * Note limitation is that we cannot have multiple passports per user
     *
     * @return Boolean|Passport
     */
    public function getPassport()
    {
        if (!$this->owner->OAuthSource === Helper::get_authenticator_name()) {
            return false;
        }

        // NOTE: cannot handle multiple passports yet
        if (!$passport = $this->owner->Passports()->first()) {
            return false;
        }

        return $passport;
    }
    
    /**
     * Get Azure ID for user
     *
     * @return Boolean|string
     */
    public function getAzureID()
    {
        if (!$passport = $this->owner->getPassport()) {
            return false;
        }

        return $passport->Identifier;
    }

    /**
     * Store token for the user
     *
     * @param string $token
     * @return void
     */
    public function storeAccessToken(string $token)
    {
        $passport = $this->owner->getPassport();

        if (!$passport) {
            return false;
        }

        $passport->AccessToken = $token;
        $passport->write();
    }

    /**
     * get the passport for the current user
     *
     * @return AccessToken
     */
    public function getAccessToken()
    {
        if (!$passport = $this->owner->getPassport()) {
            return false;
        }

        return $passport->AccessToken;
    }

    /**
     * Get the passport given an azure token
     *
     * @param AccessToken $token
     * @param Provider $provider
     * @return void
     */
    public function getPassportByToken($token, $provider)
    {
        $user = $provider->getResourceOwner($token);
        $data = $user->toArray();
        $id = $data['uid'];

        $passport = $this->owner
            ->Passports()
            ->filter('Identifier', $id)
            ->first();

        return  $passport;
    }

    /**
     * Determine if the user can edit their SSO password
     * - only local azure users can do this, not Google, MS365
     *
     * @return string
     */
    public function CanEditAzurePassword()
    {
        if (!$id = $this->getAzureID()) {
            return false;
        }

        $passport = $this->owner->Passports()
            ->filter('Identifier', $id)
            ->first();

        // @TODO check this is appropriate
        // if IDP exists in any of these, then account was logged in via local azure account,
        // and not via google, ms365

        $markers = [
            'azure' => ['', null, 'azureb2c', 'azure', 'local'], // default is 'azureb2c'
        ];

        return !$passport->IDP || in_array($passport->IDP, $markers['azure']);
    }
}

<?php

namespace SSO\Extension;

use SilverStripe\ORM\FieldType\DBBoolean;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;

/**
 * Adds a way to enable/disable the SSO Login header in templates
 */
class SiteConfigExtension extends DataExtension
{
    private static $db = [
        'EnableSSOHeader' => DBBoolean::class
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab(
            'Root.SSO',
            [
                CheckboxField::create('EnableSSOHeader', 'Enable SSO (login/logout) Header'),
            ]
        );
    }
}

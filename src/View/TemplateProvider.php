<?php

namespace SSO\View;

use SilverStripe\View\TemplateGlobalProvider;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\Director;
use SSO\Control\Controller;

/**
 * Provide templayte varaibles to the site
 */
class TemplateProvider implements TemplateGlobalProvider
{
    /**
     * Get the local AzureProvider login url
     * (which in turn redirect to azure login)
     *
     * @return string
     */
    public static function getSSOLoginURL()
    {
        $url = Config::inst()->get(Controller::class, 'login_url');
        $url = self::append_back_url($url);

        return $url;
    }

    /**
     * Get the local AzureProvider logout url
     * (which in turn redirect to azure logout)
     *
     * @return string
     */
    public static function getSSOLogoutURL()
    {
        $url = Config::inst()->get(Controller::class, 'logout_url');
        $url = self::append_back_url($url);
        return $url;
    }

    /**
     * Append the current BackURL from session
     * as a query parameter to the given string
     *
     * @param string $url
     * @return string
     */
    public static function append_back_url($url)
    {
        // Current URL is the back url
        if (Controller::has_curr()) {
            if (Controller::curr()->RedirectToHomeOnLogout) {
                $backURL = Director::absoluteBaseURL();
            } else {
                $backURL = Controller::curr()->getRequest()->getURL(true);
                $backURL = Director::absoluteURL($backURL);
            }

            // enforce https
            $backURL = preg_replace("/^http:/i", "https:", $backURL);

            // ensure always forward slash
            $backURL = rtrim($backURL, '/') . '/';

            // add new query or append to existing queries
            $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . 'BackURL=' . urlencode($backURL);
        }

        return $url;
    }

    /**
     * Get the Account URL and expose it's URL in all templates
     * Note this needs to be cross-site. so will need to be hard coded
     *
     * @return string
     */
    public static function getEditAccountURL()
    {
        // these are hardcoded as Mbie35 wont have a URL
        $url = Config::inst()->get('account', 'account_url');
        return $url;
    }

    /**
     * List of template variables
     *
     * @return array
     */
    public static function get_template_global_variables()
    {
        return [
            'SSOLoginURL' => 'getSSOLoginURL',
            'SSOLogoutURL' => 'getSSOLogoutURL',
            'EditAccountURL' => 'getEditAccountURL',
        ];
    }
}

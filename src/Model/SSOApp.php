<?php

namespace SSO\Model;

use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\ORM\FieldType\DBHTMLText;

/**
 * Model to store Azure Apps.
 * We use this to send different Welcome emails depending on
 * where the Azure user started their signup from (mbie35 or mbie9)
 * Each App has several URLs eg live vs UAT that are managed in the SSOAppURL model
 */
class SSOApp extends DataObject
{
    private static $db = [
        'Name' => DBVarchar::class,
        'AppID' => DBVarchar::class,
        'WelcomeEmailSubject' => DBVarchar::class,
        'WelcomeEmailContent' => DBHTMLText::class
    ];

    private static $many_many = [
        'AllowedURLs' => SSOAppURL::class
    ];

    private static $summary_fields = [
        'ID' => 'ID',
        'Name' => 'Name',
        'AppID' => 'Azure App ID',
        'NiceAllowedURLS' => 'Allowed URLs'
    ];

    private static $table_name = 'SSOApp';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->dataFieldByName('Name')
            ->setDescription('Name of the App (e.g. \'Kiwi Business Boost\')');

        $fields->dataFieldByName('AppID')
            ->setDescription('The App ID from Azure. AKA \'AUD\' (\'Audience Field\') or \'client_id\'');

        $fields->dataFieldByName('WelcomeEmailSubject')
            ->setDescription('Welcome email subject line for people who have been created via this app');

        $fields->dataFieldByName('WelcomeEmailContent')
            ->setDescription('Welcome email content for people who have been created via this app');

        if (!$this->isInDB()) {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create('AllowedURLsWarning', "<p class=\"message warning\">Please save before adding Allowed URL's.</p>")
            );

            return $fields;
        }

        $urls = $fields->dataFieldByName('AllowedURLs');

        $fields->removeByName('AllowedURLs');
        $fields->addFieldToTab('Root.Main', $urls);
        $urls->setTitle('Allowed Domains and Back URLs');
        $urls->getConfig()
            ->getComponentByType(GridFieldAddNewButton::class)
            ->setButtonName('Add allowed URL');

        return $fields;
    }

    /**
     * Display a friendly list of URLs for this object
     *
     * @return void
     */
    public function getNiceAllowedURLS()
    {
        return implode(', ', $this->AllowedURLs()->column('URL'));
    }
}

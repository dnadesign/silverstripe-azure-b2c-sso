<?php

namespace SSO\Model;

use SilverStripe\ORM\ValidationResult;
use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\ORM\DataObject;

/**
 * Model to hold the dom,ains for an SSOApp Dataobject
 */
class SSOAppURL extends DataObject
{
    private static $db = [
        'Title' => DBVarchar::class,
        'URL' => DBVarchar::class
    ];

    private static $table_name = 'SSOAppURL';

    private static $summary_fields = [
        'ID' => 'ID',
        'Title' => 'Title',
        'URL' => 'URL'
    ];

    /**
     * Validate the URL to wensurte it is a URL
     *
     * @return void
     */
    public function validate()
    {
        $result = ValidationResult::create();

        if (!$this->URL) {
            $result->addFieldError('URL', 'Please add a URL');
        }

        if (!filter_var($this->URL, FILTER_VALIDATE_URL)) {
            $result->addFieldError('URL', 'Invalid URL');
        }

        return $result;
    }
}

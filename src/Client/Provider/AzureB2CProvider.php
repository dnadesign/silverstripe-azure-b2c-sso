<?php

namespace SSO\Client\Provider;

use TheNetworg\OAuth2\Client\Provider\AzureResourceOwner;
use TheNetworg\OAuth2\Client\Provider\Azure;
use TheNetworg\OAuth2\Client\Grant\JwtBearer;
use SilverStripe\Security\Security;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\HTTPRequest;
use SSO\Helper\Helper;
use SSO\Client\Token\AccessToken;
use Psr\Http\Message\ResponseInterface;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Grant\AbstractGrant;
use Firebase\JWT\JWT;
use Firebase\JWT\JWK;

class AzureB2CProvider extends Azure
{
    protected $openIdConfiguration;

    public $options = [];

    public $configUrl = '';

    /**
     * Ensure that the correct scopes are used
     *
     * @param array $options
     * @param array $collaborators
     */
    public function __construct(array $options = [], array $collaborators = [])
    {
        parent::__construct($options, $collaborators);

        $this->options = $options;

        if (isset($options['configUrl'])) {
            $this->configUrl = $options['configUrl'];
        }

        if (isset($options['scopes'])) {
            $this->scope = array_merge($options['scopes'], $this->scope);
        }

        if (isset($options['defaultEndPointVersion']) &&
            in_array($options['defaultEndPointVersion'], self::ENDPOINT_VERSIONS, true)) {
            $this->defaultEndPointVersion = $options['defaultEndPointVersion'];
        }

        $this->grantFactory->setGrant('jwt_bearer', new JwtBearer());
    }

    /**
     * Get the configuration options from the well-known endpoint
     * Override the Azure::getOpenIdConfiguration in order to skip
     * checks that dont apply to Azure B2C OAuth
     *
     * @param string $tenant
     * @param string $version
     */
    protected function getOpenIdConfiguration($tenant, $version)
    {
        $openIdConfigurationUri = $this->configUrl;

        $factory = $this->getRequestFactory();
        $request = $factory->getRequestWithOptions(
            'get',
            $openIdConfigurationUri,
            []
        );

        $response = $this->getParsedResponse($request);
        $this->openIdConfiguration[$tenant][$version] = $response;

        return $this->openIdConfiguration[$tenant][$version];
    }

    /**
     * Obtain URL for logging out the user.
     *
     * @param $post_logout_redirect_uri string The URL which the user should be redirected to after logout
     * @param  $memberID int The ID of ther member to logout - required for accessToken validation
     *
     * @return string
     */
    public function getLogoutUrl($options = [])
    {
        $base = $this->getBaseLogoutUrl();
        $params = $this->getLogoutParameters($options);
        $query  = $this->buildQueryString($params);
        return $this->appendQuery($base, $query);
    }

    /**
     * Configure the URL params for the logout URL
     *
     * @param array $options
     * @return array
     */
    public function getLogoutParameters($options)
    {
        if (empty($options['state'])) {
            $options['state'] = $this->getRandomState();
        }

        // Store the state as it may need to be accessed later on.
        $this->state = $options['state'];

        // add id_token_hint - this is the stored access token for the logged in user
        $member = Security::getCurrentUser();
        $options['id_token_hint'] = $member->getAccessToken();

        $options['post_logout_redirect_uri'] = Helper::getRedirectUri();

        return $options;
    }

    /**
     * Get the base Logout URL
     * This is simialr to how the auth (login) url works
     * It is retrieved from the config endpoint for the azure b2c instance
     *
     * @return void
     */
    public function getBaseLogoutUrl()
    {
        $openIdConfiguration = $this->getOpenIdConfiguration($this->tenant, $this->defaultEndPointVersion);
        return $openIdConfiguration['end_session_endpoint'];
    }

    /**
     * Obtain a link to the Azure B2C self-service reset password policy
     * password reset self-service URL will be something like:
     *
     * @return string
     */
    public function getResetPasswordUrl($backURL = null)
    {
        $openIdConfiguration = $this->getOpenIdConfiguration($this->tenant, $this->defaultEndPointVersion);
        $authorizationURL = $openIdConfiguration['authorization_endpoint'];
        $parts = explode('?', $authorizationURL);

        $query = [
            'p' => $this->options['resetPasswordPolicy'],
            'client_id' => Helper::get_client_id(),
            'nonce' => 'defaultNonce',
            'scope' => 'openid',
            'response_type' => 'id_token',
            'prompt' => 'login'
        ];

        $resetPasswordURL = $parts[0] . '?' . http_build_query($query);

        if (!empty($backURL)) {
            $backURL = preg_replace("/^http:/i", "https:", $backURL);

            // @TODO unsure what the redirect_uri is called
            $resetPasswordURL .= (parse_url($resetPasswordURL, PHP_URL_QUERY) ? '&' : '?') .
                'redirect_uri=' . $backURL;
        }

        return $resetPasswordURL;
    }

    /**
     * Obtain a link to the Azure B2C self-service reset MFA policy
     * MFA reset self-service URL will be something like:
     *
     * @return string
     */
    public function getResetMFAUrl()
    {
        $openIdConfiguration = $this->getOpenIdConfiguration($this->tenant, $this->defaultEndPointVersion);
        $authorizationURL = $openIdConfiguration['authorization_endpoint'];
        $parts = explode('?', $authorizationURL);

        $query = [
            'p' => $this->options['resetMFAPolicy'],
            'client_id' => Helper::get_client_id(),
            'nonce' => 'defaultNonce',
            'scope' => 'openid',
            'response_type' => 'id_token',
            'prompt' => 'login'
        ];

        $resetMFAURL = $parts[0] . '?' . http_build_query($query);
        return $resetMFAURL;
    }

    /**
     * Validate the access token you received in your application.
     *
     * @param $accessToken string The access token you received in the authorization header.
     *
     * @return array
     */
    public function validateAccessToken($accessToken)
    {
        $keys = $this->getJwtVerificationKeys();

        // add 10 seconds leeway
        JWT::$leeway = 10;
        $tokenClaims = (array)JWT::decode($accessToken, JWK::parseKeySet($keys), ['RS256']);

        $this->validateTokenClaims($tokenClaims);

        return $tokenClaims;
    }

    /**
     * Get JWT verification keys from Azure Active Directory.
     *
     * @return array
     */
    public function getJwtVerificationKeys()
    {
        $request = Injector::inst()->get(HTTPRequest::class);
        $session = $request->getSession();

        if ($jwks = $session->get('JWKs')) {
            return $jwks;
        }

        $openIdConfiguration = $this->getOpenIdConfiguration($this->tenant, $this->defaultEndPointVersion);
        $keysUri = $openIdConfiguration['jwks_uri'];

        $factory = $this->getRequestFactory();
        $request = $factory->getRequestWithOptions('get', $keysUri, []);

        $jwks = $this->getParsedResponse($request);

        $session->set('JWKs', $jwks);

        return $jwks;
    }

    /**
     * Validate the access token claims from an access token you received in your application.
     *
     * @param $tokenClaims array The token claims from an access token you received in the authorization header.
     *
     * @return void
     */
    public function validateTokenClaims($tokenClaims)
    {
        if ($this->getClientId() != $tokenClaims['aud']) {
            throw new \RuntimeException('The client_id / audience is invalid!');
        }

        // removed as Additional expiry validation is being performed in firebase/JWT itself
        // if ($tokenClaims['nbf'] > time() || $tokenClaims['exp'] < time()) {
        // throw new \RuntimeException('The id_token is invalid!');
        // }

        $version = array_key_exists('ver', $tokenClaims) ? $tokenClaims['ver'] : $this->defaultEndPointVersion;
        $tenant = $this->getTenantDetails($this->tenant, $version);

        if ($tokenClaims['iss'] != $tenant['issuer']) {
            throw new \RuntimeException('Invalid token issuer (tokenClaims[iss]' . $tokenClaims['iss'] . ', tenant[issuer] ' . $tenant['issuer'] . ')!');
        }
    }

    /**
     * Inherited
     *
     * @param [type] $version
     * @return void
     */
    protected function getVersionUriInfix($version)
    {
        return
            ($version == self::ENDPOINT_VERSION_2_0)
                ? '/v' . self::ENDPOINT_VERSION_2_0
                : '';
    }

    /**
     * Inherited
     *
     * @param ResponseInterface $response
     * @param [type] $data
     * @return void
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        // if ($response->getStatusCode()) {
        if ($response->getStatusCode() === 200) {
            return;
        }

        if (isset($data['odata.error']) || isset($data['error'])) {
            if (isset($data['odata.error']['message']['value'])) {
                $message = $data['odata.error']['message']['value'];
            } elseif (isset($data['error']['message'])) {
                $message = $data['error']['message'];
            } elseif (isset($data['error']) && !is_array($data['error'])) {
                $message = $data['error'];
            } else {
                $message = $response->getReasonPhrase();
            }

            if (isset($data['error_description']) && !is_array($data['error_description'])) {
                $message .= PHP_EOL . $data['error_description'];
            }

            throw new IdentityProviderException(
                $message,
                $response->getStatusCode(),
                $response
            );
        }
    }

    /**
     * Inherited
     *
     * @return void
     */
    protected function getDefaultScopes()
    {
        return $this->scope;
    }

    /**
     * Inherited
     *
     * @return void
     */
    protected function getScopeSeparator()
    {
        return $this->scopeSeparator;
    }

    /**
     * Inherited
     *
     * @return void
     */
    protected function createAccessToken(array $response, AbstractGrant $grant)
    {
        return new AccessToken($response, $this);
    }

    /**
     * Inherited
     *
     * @return void
     */
    protected function createResourceOwner(array $response, \League\OAuth2\Client\Token\AccessToken $token)
    {
        return new AzureResourceOwner($response);
    }

    /**
     * Inherited
     *
     * @return void
     */
    private function wrapResponse($response)
    {
        if (empty($response)) {
            return;
        } elseif (isset($response['value'])) {
            return $response['value'];
        }

        return $response;
    }
}

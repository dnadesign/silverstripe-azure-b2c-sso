<?php

namespace  SSO\Client\Handler;

use SilverStripe\Security\IdentityStore;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\HTTPRequest;

class LogoutHandler
{
    public function handleLogout(HTTPRequest $request)
    {
        Injector::inst()->get(IdentityStore::class)->logOut($request);
    }
}

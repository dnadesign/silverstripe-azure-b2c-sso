<?php

namespace  SSO\Client\Handler;

use SilverStripe\View\Requirements;
use SilverStripe\ErrorPage\ErrorPage;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Core\Convert;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\Controller;
use SSO\Helper\Helper;
use Psr\Log\LoggerInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use Bigfork\SilverStripeOAuth\Client\Handler\ErrorHandler;

/**
 * Bespoke Error handler
 * 1) Redirect the user to a bespoke URL if they cancel signup
 * 2) Capture errors from specific IDP's and display freidnly error messages
 * (specific requirement from DIA)
 * 3) Detect a password reset request adnf redirect the user to the bespoke password
 * for our implementation of Azure B2C
 *
 */
class LoginErrorHandler implements ErrorHandler
{
    use Configurable;

    /**
     * Specific error code passed back to /callback if the user cancells signup
     * (or any other form)
     */
    const TRIGGER_CANCELLED_SIGNUP = 'AADB2C90091';

    /**
     * Specific error code passed back to /callback if the user triggers the
     * reset password policy
     */
    const TRIGGER_PASSWORD_MESSAGE = 'AADB2C90118';

    /**
     * Custom Error handler for Azure
     * @todo add to config
     *
     * @param AbstractProvider $provider
     * @param HTTPRequest $request
     * @param Exception $exception
     * @return void
     */
    public function handleError(AbstractProvider $provider, HTTPRequest $request, \Exception $exception)
    {
        $errorDescription = $request->getVar('error_description');
        $controller = Controller::curr();
        
        // Check for known exception codes
        // note this logs its own message
        if ($errorDescription) {
            if ($message = $this->checkKnownExceptions($errorDescription)) {
                return $this->errorPage($message);
            }

            // Trigger reset a hotjar questionaire flow if a 'cancel' signup message detected from azure
            if ($redirect = $this->checkCancelledSignup($errorDescription, $request)) {
                return $this->redirect($redirect, $request);
            }

            // Trigger reset password flow if message detected from azure
            if ($redirect = $this->checkPasswordReset($errorDescription, $request)) {
                return $this->redirect($redirect, $request, $request);
            }
        }

        $message = $exception ? $exception->getMessage() : 'An unknown error occured';
        
        $logText = sprintf(
            "B2C exception: %s",
            $message
        );
        Injector::inst()->get(LoggerInterface::class)->error($logText);

        return $controller->httpError(500, $message);
    }

    /**
     * return an error page that uses the ErrorPage template
     * but has a 200 status to avoid the exception handling on CWP environments
     *
     * @param string $message
     * @return void
     */
    public function errorPage($message)
    {
        $errorPage = ErrorPage::get()
            ->filter(array(
                "ErrorCode" => 500
            ))->first();

        if ($errorPage) {
            Requirements::clear();
            Requirements::clear_combined_files();
            
            // Dev environments will have the error message added regardless of template changes
            $errorPage->Content = "\n<p>" . Convert::raw2xml($message) ."</p>";

            $body = $errorPage->renderWith('SilverStripe\ErrorPage\ErrorPage');
            $response = new HTTPResponse;
            $response->setStatusCode(200);
            $response->setBody($body);

            return $response;
        }
    }

    /**
     * redirect
     */
    public function redirect($redirect, $request)
    {
        $controller = Controller::curr();

        // workaround for the 'Already redirected to' issue
        $controller->getResponse()->addHeader('Location', $redirect);
        // need to restart session, otherwise we will get the invalid session state error
        $request->getSession()->restart($request);

        return $controller->redirect($redirect);
    }

    /**
     * Check if the error message contains the code that represents a cancelled user signup.
     * Then redirect the user back to the redirect_uri in the URL with an appedned query param
     * for &cancelledsignup=1
     *
     * @param string $errorDescription
     * @param HTTPRequest $request
     * @return mixed
     */
    public function checkCancelledSignup($errorDescription, HTTPRequest $request)
    {
        if (strpos($errorDescription, self::TRIGGER_CANCELLED_SIGNUP) !== false) {
            if ($backURL = $request->getSession()->get('oauth2.backurl')) {
                $backURL = str_replace(['?cancelledsignup=1', '&cancelledsignup=1'], '', $backURL);
                $backURL .= (parse_url($backURL, PHP_URL_QUERY) ? '&' : '?') . 'cancelledsignup=1';

                return $backURL;
            }
        }

        return false;
    }

    /**
     * Check if the Azure B2C password reset policy flow has been triggered
     * If  a user click forgot password on the Azure B2C login screen, Azure will
     * trigger an exception, and the user will be sent back to the site with
     * error_description in the query params. We can detect this and then trigger the proper
     * azure b2c self-service password reset flow.
     *
     * @param string $errorDescription
     * @param HTTPRequest $request
     * @return mixed
     */
    public function checkPasswordReset($errorDescription, HTTPRequest $request)
    {
        if (strpos($errorDescription, self::TRIGGER_PASSWORD_MESSAGE) !== false) {
            return Helper::getProviderResetPasswordURL($request);
        }

        return false;
    }

    /**
     * Check if the request contains a known error code.
     * Realme for isntance, will return long error codes in the error_description
     * in the query params. RealMe docs give us these codes, so we can use a config
     * lookup table to display friendly messages
     *
     * @param string $errorDescription
     * @return mixed
     */
    public function checkKnownExceptions($errorDescription)
    {
        $lookup = $this->config()->get('lookup');
        $message = null;

        if ($lookup) {
            foreach ($lookup as $code => $text) {
                if (strpos($errorDescription, $code) !== false) {
                    $message = $text;

                    // $logText = sprintf(
                    //     "B2C exception: %s | %s | %s",
                    //     $code,
                    //     $text,
                    //     $errorDescription
                    // );
                    // Injector::inst()->get(LoggerInterface::class)->error($logText);

                    return $message;
                }
            }
        }

        return $message;
    }
}

<?php

namespace  SSO\Client\Handler;

use UserAPI\Handler\MemberHandler;
use SilverStripe\Security\Security;
use SilverStripe\Security\Member;
use SilverStripe\Security\IdentityStore;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Control\Session as SilverstripeSession;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\Controller;
use SSO\Control\Session;
use SSO\Client\Token\AccessToken;
use SSO\Client\Mapper\AzureMemberMapper;
use League\OAuth2\Client\Provider\AbstractProvider;
use Bigfork\SilverStripeOAuth\Client\Handler\LoginTokenHandler as BigforkLoginTokenHandler;

/**
 * Override the Bigfork token handlewr to asdd further, bespoke functionality
 */
class LoginTokenHandler extends BigforkLoginTokenHandler
{
    /**
     * Handle token from Azure
     * See the docs at @link https://github.com/bigfork/silverstripe-oauth#usage for how the
     * handler gets triggered
     *
     * Override the BigForm module so we can set a session
     * to ensure the HTTPMiddleware doesnt redirect
     *
     * @param AccessToken $token
     * @param AbstractProvider $provider
     * @return HTTPResponse|null
     */
    public function handleToken($token, $provider)
    {
        try {
            // Find or create a member from the token
            $member = $this->findOrCreateMember($token, $provider);
        } catch (\Exception $e) {
            // @TODO better error reporting, as this actually results in a continuous loop
            echo $e->getMessage();
            exit;
            return Security::permissionFailure(null, $e->getMessage());
        }

        // Check whether the member can log in before we proceed
        $result = $member->validateCanLogin();
        if (!$result->isValid()) {
            return Security::permissionFailure(null, implode('; ', $result->getMessages()));
        }

        // Log the member in
        $identityStore = Injector::inst()->get(IdentityStore::class);
        $identityStore->logIn($member);

        // save the members azure access token
        $member->storeAccessToken($token->getToken());

        // post login actions
        // use a session to skip checking sso cookie on return url
        $request = Injector::inst()->get(HTTPRequest::class);
        $session = $request->getSession();
        $session->set('sso_session_action', true);

        // set Cookie for subdomains
        $SSOSession = Injector::inst()->create(Session::class);
        if (!$SSOSession->getCookie()) {
            $SSOSession->setCookie($token, $request);
        }

        return null;
    }

    /**
     * Custom fucntionality to find of create a memebr
     *
     * @param AccessToken $token
     * @param AbstractProvider $provider
     * @return Member
     */
    protected function findOrCreateMember($token, $provider)
    {
        $user = $provider->getResourceOwner($token);
        $data = $user->toArray();

        // Note the implementation of MBIE azure doesnt have oid as a claim,
        // instead it uses 'sub' in the id_toke which apprently is the same
        // $id = $user->getId();
        $id = $data['sub'];

        $handler = Injector::inst()->create(MemberHandler::class);
        $member = $handler->findOrCreateMember($id, $data);

        return $member;
    }

    /**
     * Override createMember to handle the case when a user creates a new Azure user
     * using an email that exists in Silverstripe (Remember azure makes users verufy their email address).
     *
     * @param AccessToken $token
     * @param AbstractProvider $provider
     * @return void
     */
    protected function createMember($token, $provider)
    {
        $resourceOwner = $provider->getResourceOwner($token);
        $array = $resourceOwner->toArray();

        // if a user already exists, uodate with new info
        if (isset($array['emails']) && isset($array['emails'][0])) {
            $member = Member::get()
                ->filter('Email', $array['emails'][0])
                ->first();

            if ($member) {
                return $this->updateMember($member, $token, $provider);
            }
        }

        $member = parent::createMember($token, $provider);

        return $member;
    }

    /**
     * Update the incoming member to ensure that azure is the source of truth
     *
     * @param Member $member
     * @param AccessToken $token
     * @param AbstractProvider $provider
     * @return void
     */
    protected function updateMember(Member $member, $token, $provider)
    {
        $session = $this->getSession();
        $providerName = $session->get('oauth2.provider');
        $user = $provider->getResourceOwner($token);

        $member = $this->getMapper($providerName)->map($member, $user);
        $member->OAuthSource = $providerName;

        // only write if required
        // also precvents other fields inadvertantly being overwritten
        $fieldsToCheck = ['FirstName', 'Surname', 'Email'];
        $isChanged = false;
        foreach ($fieldsToCheck as $field) {
            if ($member->isChanged($field)) {
                $isChanged = true;
                break;
            }
        }

        if ($isChanged) {
            $member->write();
        }

        return $member;
    }

    /**
     * Get the array that maps Silverstripe field names to Azure B2C field names
     * eg 'FirstName' => 'given_name'
     *
     * @param AbstractProvider $providerName
     * @return AzureMemberMapper
     */
    protected function getMapper($providerName)
    {
        return Injector::inst()->createWithArgs(AzureMemberMapper::class, [$providerName]);
    }

    /**
     * Get the current session o
     *
     * @return Session
     */
    protected function getSession()
    {
        if (Controller::has_curr()) {
            return Controller::curr()->getRequest()->getSession();
        }

        return Injector::inst()->create(SilverstripeSession::class, isset($_SESSION) ? $_SESSION : []);
    }
}

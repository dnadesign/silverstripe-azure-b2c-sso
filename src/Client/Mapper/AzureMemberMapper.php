<?php

namespace SSO\Client\Mapper;

use SilverStripe\Security\Member;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Bigfork\SilverStripeOAuth\Client\Mapper\GenericMemberMapper;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Injector\Injectable;

class AzureMemberMapper extends GenericMemberMapper
{
    use Configurable;

    use Injectable;

    /**
     * Mapping of SS Field => Azure B2C fields
     *
     * @var array
     */
    private static $mapping = [
        'Email' => 'email',
        'FirstName' => 'given_name',
        'Surname' => 'family_name'
    ];

    public function __construct()
    {
        // no-op
    }

    /**
     * Map incoming Azure data to Silverstripe Member fields
     * Note the special treatment for Email
     *
     * @param Member $member
     * @param Array $resourceOwner
     * @return Member
     */
    public function map(Member $member, $data)
    {
        if ($data instanceof ResourceOwnerInterface) {
            $data = $data->toArray();
        }

        $mapping = $this->getMapping();

        foreach ($mapping as $target => $source) {
            if (array_key_exists($source, $data)) {
                $member->$target = $data[$source];
            }
        }

        return $member;
    }

    /**
     * return the custom mapping array
     *
     * @return array
     */
    public function getMapping()
    {
        return self::config()->uninherited('mapping');
    }
}

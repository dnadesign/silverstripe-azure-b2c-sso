<?php

namespace SSO\Client\Token;

use RuntimeException;
use League\OAuth2\Client\Token\AccessToken as LeagueAccesstoken;
use League\OAuth2\Client\Provider\AbstractProvider;
use Firebase\JWT\JWT;
use Firebase\JWT\JWK;

class AccessToken extends LeagueAccesstoken
{
    /**
     * @var string
     */
    protected $idToken;

    /**
     * @var string
     */
    protected $idTokenClaims;

    /**
     * Override TheNetworg\OAuth2\Client\Token constructor
     * as we need to change the way the JWT is parsed
     *
     * @param array $options
     * @param AbstractProvider $provider
     * @return void
     */
    public function __construct(array $options, $provider)
    {
        parent::__construct($options);

        if (!empty($options['id_token'])) {
            $this->idToken = $options['id_token'];

            $keys = $provider->getJwtVerificationKeys();

            // SSO TODO EXTEND THIS
            $idTokenClaims = null;
            try {
                $tks = explode('.', $this->idToken);
                // Check if the id_token contains signature
                if (3 == count($tks) && !empty($tks[2])) {
                    JWT::$leeway = 10;
                    $idTokenClaims = (array)JWT::decode($this->idToken, JWK::parseKeySet($keys), ['RS256']);
                } else {
                    // The id_token is unsigned (coming from v1.0 endpoint) - https://msdn.microsoft.com/en-us/library/azure/dn645542.aspx

                    // Since idToken is not signed, we just do OAuth2 flow without validating the id_token
                    // Validate the access_token signature first by parsing it as JWT into claims
                    // $accessTokenClaims = (array)JWT::decode($options['access_token'], $keys, ['RS256']);
                    // Then parse the idToken claims only without validating the signature
                    $idTokenClaims = (array)JWT::jsonDecode(JWT::urlsafeB64Decode($tks[1]));
                }
            } catch (\Exception $e) { // renamed from JWT_Exception
                
                // @todo proper error message
                var_dump($e->getMessage());
                exit;
                throw new RuntimeException('Unable to parse the id_token!');
            }

            $provider->validateTokenClaims($idTokenClaims);

            $this->idTokenClaims = $idTokenClaims;
        }
    }

    /**
     * Getter for the list of token claims
     *
     * @return string
     */
    public function getIdTokenClaims()
    {
        return $this->idTokenClaims;
    }
}

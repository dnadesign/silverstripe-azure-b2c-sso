<?php

namespace SSO\Helper;

use SilverStripe\Core\Injector\Injector;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\Director;
use League\OAuth2\Client\Provider\AbstractProvider;
use Bigfork\SilverStripeOAuth\Client\Helper\Helper as BigforkOAuthHelper;
use Bigfork\SilverStripeOAuth\Client\Factory\ProviderFactory;
use Bigfork\SilverStripeOAuth\Client\Authenticator\Authenticator;
use SilverStripe\Core\Environment;
use SilverStripe\Control\Controller as SilverStripeController;
use SSO\Control\Controller;

/**
 * Helper class with various common methods
 */
class Helper extends BigforkOAuthHelper
{
    use Configurable;

    /**
     * Name of the Authenticator for quick lookup
     *
     * @var string
     */
    public static $authenticator_name = 'AzureB2CProvider';

    /**
     * Get the authenticator name static
     *
     * @return string
     */
    public static function get_authenticator_name()
    {
        return self::$authenticator_name;
    }

    /**
     * Get the client ID of for Azure b2C from Environment Variable
     *
     * @return string
     */
    public static function get_client_id()
    {
        return Environment::getEnv('AZUREB2C_CLIENT_ID');
    }

    /**
     * Ensure Auth URL is https
     *
     * @param string $provider The OAuth provider name (as configured in YAML)
     * @param string $context The context from which the token is being requested, e.g. 'login'
     * @param array $scopes An array of OAuth "scopes" required
     * @return string
     */
    public static function buildAuthorisationUrl($provider, $context = '', array $scopes = [])
    {
        $url = parent::buildAuthorisationUrl($provider, $context, $scopes);
        $url = preg_replace("/^http:/i", "https:", $url);
        return $url;
    }

    /**
     * Undocumented function
     *
     * @param string|HTTPRequest $request
     * @return void
     */
    private static function setBackURL($request = null)
    {
        // set a back URL in the session
        if (is_string($request)) {
            $backURL = $request;
        } elseif ($request instanceof HTTPRequest) {
            $backURL = self::findBackURL($request);
        } else {
            $backURL = Director::absoluteBaseURL();
        }
        
        $session = ($request instanceof HTTPRequest)
                    ? $request->getSession()
                    : Controller::curr()->getRequest()->getSession();
        
        $session->set('oauth2.backurl', $backURL);
    }

    /**
     * Generate a link to Login at azure
     *
     * @param mixed $request either a backURL string,
     *      or a request to extract the current URL from,
     *      or null to use the home page as the backURL
     * @return string
     */
    public static function getProviderLoginURL($request = null)
    {
        $config = self::getProviderconfig();
        $scope = isset($config['scopes']) ? $config['scopes'] : ['email'];
        $url = self::buildAuthorisationUrl(self::$authenticator_name, 'login', $scope);

        // set backUR:
        self::setBackURL($request);
        return $url;
    }

    /**
     * Generate a link to logout at Azure
     *
     * @param HTTPRequest $request
     * @return string
     */
    public static function getProviderLogoutURL($request = null)
    {
        $config = self::getProviderconfig();
        $scope = isset($config['scopes']) ? $config['scopes'] : ['email'];
        $url = self::buildAuthorisationUrl(self::$authenticator_name, 'logout', $scope);

        self::setBackURL($request);
        return $url;
    }

    /**
     * @param string $provider The OAuth provider name (as configured in YAML)
     * @param string $context The context from which the token is being requested, e.g. 'login'
     * @param array $scopes An array of OAuth "scopes" required
     * @return string
     */
    public static function getProviderResetPasswordURL(HTTPRequest $request)
    {
        // set back url for the azure redirect uri
        $backURL = self::findBackURL($request);

        // get url from Azure
        $provider = self::getProvider(); // AbstractProvider
        $url = $provider->getResetPasswordUrl($backURL);
        return $url;
    }

    /**
     * @param string $provider The OAuth provider name (as configured in YAML)
     * @param string $context The context from which the token is being requested, e.g. 'login'
     * @param array $scopes An array of OAuth "scopes" required
     * @return string
     */
    public static function getProviderResetMFAURL(HTTPRequest $request)
    {
        // set back url for the azure redirect uri
        $backURL = self::findBackURL($request);

        // get url from Azure
        $provider = self::getProvider(); // AbstractProvider
        $url = $provider->getResetMFAUrl($backURL);
        return $url;
    }

    /**
     * Get the Azure auth provider from config
     *
     * @return AbstractProvider
     */
    public static function getProvider()
    {
        $providerFactory = Injector::inst()->get(ProviderFactory::class);
        $provider = $providerFactory->getProvider(self::$authenticator_name);

        return $provider;
    }

    /**
     * get the config for the Azure provider
     *
     * @return array
     */
    public static function getProviderconfig()
    {
        $providers = Config::inst()->get(Authenticator::class, 'providers');
        $config = $providers[self::$authenticator_name];

        return $config;
    }

    /**
     * Get the current top level Domain
     *
     * @return string
     */
    public static function get_domain()
    {
        $url = Director::absoluteBaseURL();

        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }

        return Director::host();
    }

    /**
     * Dtermine the back URL
     *
     * @param HTTPRequest $request
     * @return string|null
     */
    public static function findBackURL(HTTPRequest $request)
    {
        if ($request->requestVar('BackURL')) {
            $backURL = $request->requestVar('BackURL');
        } elseif ($request->getSession() && $request->getSession()->get('oauth2.backurl')) {
            $backURL = $request->getSession()->get('oauth2.backurl');
        } elseif ($request->getSession() && $request->getSession()->get('BackURL')) {
            $backURL = $request->getSession()->get('BackURL');
        } elseif ($request->isAjax() && $request->getHeader('X-Backurl')) {
            $backURL = $request->getHeader('X-Backurl');
        } elseif ($request->getHeader('Referer')) {
            $backURL = $request->getHeader('Referer');
        } else {
            $backURL = null;
        }

        $backURL = Director::absoluteURL($backURL);

        if (!$backURL || !Director::is_site_url($backURL)) {
            $backURL = Director::absoluteBaseURL();
        }

        $backURL = preg_replace("/^http:/i", "https:", $backURL);

        // ensure always forward slash
        $backURL = rtrim($backURL, '/') . '/';

        return $backURL;
    }

    /**
     * Add in the redirectUri option to this service's constructor options
     *
     * @param array $config
     * @return array
     */
    public static function addRedirectUriToServiceConfig(array $config)
    {
        if (!empty($config['constructor']) && is_array($config['constructor'])) {
            $key = key($config['constructor']); // Key may be non-numeric
            $config['constructor'][$key]['redirectUri'] = static::getRedirectUri();
        }

        return $config;
    }

    /**
     * override to ensute that the callbaxck URL sent to Azure is always https
     * @return string
     */
    public static function getRedirectUri()
    {
        $controller = Injector::inst()->get(Controller::class);
        $url = SilverStripeController::join_links($controller->AbsoluteLink(), 'callback/');
        $url = preg_replace("/^http:/i", "https:", $url);
        return $url;
    }
}

<?php

namespace SSO\Admin;

use SilverStripe\Admin\ModelAdmin;
use SSO\Model\SSOAppURL;
use SSO\Model\SSOApp;

/**
 * Admin to manage allowed Apps
 * Apps are defiend in azure and are those domains that are allowed to use this system
 * We use these apps to send different Welcome emails depending on the Users Identity Provider (IDP)
 */
class SSOAppModelAdmin extends ModelAdmin
{
    private static $managed_models = [
        SSOApp::class,
        SSOAppURL::class
    ];

    private static $belongs_many_many = [
        'SSOApp' => SSOApp::class
    ];

    private static $url_segment = 'sso-admin';

    private static $menu_title = 'SSO';

    private static $menu_icon_class = 'font-icon-user-lock';
}

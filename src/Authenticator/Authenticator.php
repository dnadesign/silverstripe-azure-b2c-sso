<?php

namespace SSO\Authenticator;

use SilverStripe\Security\Member;
use SilverStripe\Security\Authenticator as SilverStripeAuthenticator;
use Bigfork\SilverStripeOAuth\Client\Authenticator\Authenticator as BigforkAuthenticator;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\Controller;
use Bigfork\SilverStripeOAuth\Client\Authenticator\LoginHandler;
use SilverStripe\Security\Security;

class Authenticator extends BigforkAuthenticator
{
    /**
     * @var array
     * @config
     */
    private static $providers = [];

    /**
     * Get the supported services for this Authenticator
     *
     * @return void
     */
    public function supportedServices()
    {
        // IF currently on a Security page (eg Security/logn) then ignore this Authenticator.
        // This allows us to retain Silverstripe authentication for CMS access
        if (Controller::has_curr() && $controller = Controller::curr()) {
            if ($controller instanceof Security) {
                return 0;
            }
        }
        
        return SilverStripeAuthenticator::LOGIN;
    }

    /**
     * Creat the login Handler for this Authenticator
     *
     * @param string $link
     * @return LoginHandler
     */
    public function getLoginHandler($link)
    {
        return LoginHandler::create($link, $this);
    }

    public function authenticate(array $data, HTTPRequest $request, ValidationResult &$result = null)
    {
        // No-op
    }

    public function checkPassword(Member $member, $password, ValidationResult &$result = null)
    {
        // No-op
    }

    public function getLogoutHandler($link)
    {
        // No-op
    }

    public function getLostPasswordHandler($link)
    {
        // No-op
    }

    public function getChangePasswordHandler($link)
    {
        // No-op
    }
}

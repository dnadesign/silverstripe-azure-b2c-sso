<% if $SiteConfig.EnableSSOHeader %>
    <div class="ssoheader">
        <div class="ssoheader-wrapper">
            <% if $CurrentMember %>
                <p class="ssoheader-welcome">Hi $CurrentMember.FirstName</p>
                <a class="ssoheader-link" href="$EditAccountURL" target="_blank">Edit account</a>
                <a class="ssoheader-link" href="$SSOLogoutURL">Logout</a>
            <% else %>
                <a class="ssoheader-link" href="$SSOLoginURL">Login or sign up</a>
            <% end_if %>
        </div>
    </div>
<% end_if %>